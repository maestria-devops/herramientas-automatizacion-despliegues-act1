# crea una cuenta para wordpress
accounts::user { 'wp':
  uid      => '4001',
  gid      => '4001',
  shell    => '/bin/bash',
  password => '!!',
  locked   => false,
}

# instala el servicio de apache
class { 'apache':
  mpm_module    => 'prefork',
  default_vhost => false,
}

# instala el mod de php
class { 'apache::mod::php':
  package_name => 'php',
}

# crea el virtual host en donde estará alojado wordpress
apache::vhost { 'wordpress':
  servername      => 'wordpress',
  port            =>  80,
  docroot         => '/var/www/wordpress',
  docroot_owner   => 'wp',
  docroot_group   => 'wp',
}

# instala algunos otros paquetes necesarios para wordpress
package { ['php-mysql', 'php-curl', 'php-gd', 'php-mbstring', 'php-xml', 'php-xmlrpc', 'php-soap']:
  ensure => 'installed',
}

# instala la base de datos MySql
class { 'mysql::server':
  root_password           => 'strongpassword',
  remove_default_accounts => true,
  restart                 => true,
}

# crea el usuario de base de datos para wordpress
mysql::db { 'wp_db':
  user     => 'wp_db_user',
  password => '1234',
  host     => 'localhost',
  grant    => ['ALL'],
}

# instala una instancia de wordpress sobre el virtual host de apache anteriomente creado
# se pasan los datos adicionales para configura la instancia
class { 'wordpress' :
  settings => {
    'localhost' => {
      wproot        => '/var/www/wordpress',
      owner         => 'wp',
      dbhost        => 'localhost',
      dbname        => 'wp_db',
      dbuser        => 'wp_db_user',
      dbpasswd      => '1234',
      wpadminuser   => 'admin',
      wpadminpasswd => 'admin',
      wpadminemail  => 'foo@mydomain.com',
      wptitle       => 'Master DevOps deploy WordPress with puppet',
    }
  }
}


# geoffrey.moraes@unir.net
